#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
extern crate rocket_contrib;
extern crate serde;
extern crate petrichor;

use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;
use serde::{Serialize, Deserialize};

use petrichor::stdio_handler;

stdio_handler! {
    let kablow_handle = "/kablow" -> wrap "cat", [];
}

stdio_handler! {
    let shabam_handle = "/shabam" -> read "hope" from "echo", ["Hello", "world!"];
}

fn main() {
    rocket::ignite()
        .mount("/", StaticFiles::from(concat!(env!("CARGO_MANIFEST_DIR"), "/examples/calculator/static")))
        .mount("/", routes![shabam_handle, kablow_handle])
        .launch();
}
